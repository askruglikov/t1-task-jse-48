package ru.t1.kruglikov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.exception.entity.UserNotFoundException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "TM_SESSION")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class Session extends AbstractUserOwnedModel {

    @NotNull
    @Column(name = "LAST_DATE")
    private Date lastDate = new Date();

    @Nullable
    @Column(name = "ROLE")
    private Role role = null;

    public Session(@NotNull final User user) {
        if (user == null) throw new UserNotFoundException();
        this.setUser(user);
    }

}

