package ru.t1.kruglikov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.enumerated.Role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "TM_USER")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserDTO extends AbstractModelDTO {

    @Nullable
    @Column(name = "LOGIN")
    private String login;

    @Nullable
    @Column(name = "PASSWORD_HASH")
    private String passwordHash;

    @Nullable
    @Column(name = "EMAIL")
    private String email;

    @Nullable
    @Column(name = "FIRST_NAME")
    private String firstName;

    @Nullable
    @Column(name = "LAST_NAME")
    private String lastName;

    @Nullable
    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @NotNull
    @Column(name = "ROLE")
    private Role role = Role.USUAL;

    @NotNull
    @Column(name = "LOCKED")
    private Boolean locked = false;

    public UserDTO(
            @NotNull final String login,
            @NotNull final String passwordHash
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public UserDTO(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final String email
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    public UserDTO(
            @NotNull final String login,
            @NotNull final String passwordHash,
            @NotNull final Role role
    ) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

}