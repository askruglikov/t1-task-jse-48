package ru.t1.kruglikov.tm.exception.field;

public final class ProjectIdEmptyException extends AbstractFieldExceprion {

    public ProjectIdEmptyException() {
        super("Error! Project id is empty...");
    }

}
