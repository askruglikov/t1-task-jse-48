package ru.t1.kruglikov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.kruglikov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.kruglikov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.kruglikov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.kruglikov.tm.api.service.IConnectionService;
import ru.t1.kruglikov.tm.api.service.IPropertyService;
import ru.t1.kruglikov.tm.api.service.dto.IUserDtoService;
import ru.t1.kruglikov.tm.dto.model.UserDTO;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.exception.entity.EntityNotFoundException;
import ru.t1.kruglikov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kruglikov.tm.exception.entity.UserNotFoundException;
import ru.t1.kruglikov.tm.exception.field.EmailEmptyException;
import ru.t1.kruglikov.tm.exception.field.IdEmptyException;
import ru.t1.kruglikov.tm.exception.field.LoginEmptyException;
import ru.t1.kruglikov.tm.exception.field.PasswordEmptyException;
import ru.t1.kruglikov.tm.exception.user.ExistsEmailException;
import ru.t1.kruglikov.tm.exception.user.ExistsLoginException;
import ru.t1.kruglikov.tm.exception.user.RoleEmptyException;
import ru.t1.kruglikov.tm.repository.dto.ProjectDtoRepository;
import ru.t1.kruglikov.tm.repository.dto.SessionDtoRepository;
import ru.t1.kruglikov.tm.repository.dto.TaskDtoRepository;
import ru.t1.kruglikov.tm.repository.dto.UserDtoRepository;
import ru.t1.kruglikov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

public class UserDtoService extends AbstractDtoService<UserDTO, IUserDtoRepository> implements IUserDtoService {

    @NotNull
    private final IPropertyService propertyService;

    public UserDtoService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    public IUserDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserDtoRepository(entityManager);
    }

    public ITaskDtoRepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return new TaskDtoRepository(entityManager);
    }

    public IProjectDtoRepository getProjectRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDtoRepository(entityManager);
    }

    public ISessionDtoRepository getSessionRepository(@NotNull final EntityManager entityManager) {
        return new SessionDtoRepository(entityManager);
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return add(user);
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        return add(user);
    }

    @NotNull
    @Override
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        return add(user);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            return repository.findByLogin(login);
        } catch (@NotNull final NoResultException e) {
            return null;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            return repository.findByEmail(email);
        } catch (@NotNull final NoResultException e) {
            return null;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO removeOne(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = getRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = getTaskRepository(entityManager);
            @NotNull final IProjectDtoRepository projectRepository = getProjectRepository(entityManager);
            @NotNull final ISessionDtoRepository sessionRepository = getSessionRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.removeAll(user.getId());
            projectRepository.removeAll(user.getId());
            sessionRepository.removeAll(user.getId());
            userRepository.remove(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }

        return user;
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = getRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = getTaskRepository(entityManager);
            @NotNull final IProjectDtoRepository projectRepository = getProjectRepository(entityManager);
            @NotNull final ISessionDtoRepository sessionRepository = getSessionRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.removeAll();
            projectRepository.removeAll();
            sessionRepository.removeAll();
            userRepository.removeAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public UserDTO removeOneByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new EntityNotFoundException();
        return removeOne(user);
    }

    @Nullable
    @Override
    public UserDTO removeOneByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();

        @Nullable final UserDTO user = findByEmail(email);
        if (user == null) throw new EntityNotFoundException();
        return removeOne(user);
    }

    @NotNull
    private UserDTO update(@Nullable final UserDTO user) {
        if (user == null) throw new ProjectNotFoundException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public UserDTO setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new EntityNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));

        return update(user);
    }

    @NotNull
    @Override
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new EntityNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);

        return update(user);
    }

    @NotNull
    private UserDTO setLockOneByLogin(
            @Nullable final String login,
            @NotNull final Boolean locked
    ) {
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(locked);

        return update(user);
    }

    @NotNull
    @Override
    public UserDTO lockOneByLogin(@Nullable final String login) {
        return setLockOneByLogin(login, true);
    }

    @NotNull
    @Override
    public UserDTO unlockOneByLogin(@Nullable final String login) {
        return setLockOneByLogin(login, false);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

}
