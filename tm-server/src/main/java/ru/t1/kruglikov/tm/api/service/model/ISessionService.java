package ru.t1.kruglikov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.model.SessionDTO;
import ru.t1.kruglikov.tm.model.Session;

import java.util.Collection;
import java.util.List;

public interface ISessionService extends IUserOwnedService<Session> {
}


