package ru.t1.kruglikov.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.Connection;

public interface IConnectionService {

    @NotNull
    EntityManagerFactory factory();

    @NotNull
    EntityManager getEntityManager();

}

