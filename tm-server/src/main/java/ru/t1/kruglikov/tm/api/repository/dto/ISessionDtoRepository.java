package ru.t1.kruglikov.tm.api.repository.dto;

import ru.t1.kruglikov.tm.dto.model.SessionDTO;

public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDTO> {
}