package ru.t1.kruglikov.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.api.service.ILocatorService;
import ru.t1.kruglikov.tm.dto.model.SessionDTO;
import ru.t1.kruglikov.tm.dto.request.AbstractUserRequest;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.exception.user.PermissionException;

public abstract class AbstractEndpoint {

    @Getter @NotNull
    private final ILocatorService locatorService;

    public AbstractEndpoint(final ILocatorService locatorService) {
        this.locatorService = locatorService;
    }

    @NotNull
    protected SessionDTO check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null || role == null) throw new PermissionException();
        @Nullable final String token = request.getToken();
        @Nullable final SessionDTO session = locatorService.getAuthService().validateToken(token);
        if (session.getRole() == null) throw new PermissionException();
        if(!session.getRole().equals(role)) throw new PermissionException();
        return session;
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new PermissionException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new PermissionException();
        return locatorService.getAuthService().validateToken(token);
    }

}
