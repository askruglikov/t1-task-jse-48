package ru.t1.kruglikov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.model.TaskDTO;
import ru.t1.kruglikov.tm.enumerated.TaskSort;

import java.util.List;

public interface ITaskDtoRepository extends IUserOwnedDtoRepository<TaskDTO> {

    @Nullable
    List<TaskDTO> findAll(
            @NotNull String userId,
            @NotNull TaskSort sort
    );

    @Nullable
    List<TaskDTO> findAll(
            @NotNull TaskSort sort
    );

    @Nullable
    List<TaskDTO> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void removeTasksByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

}

