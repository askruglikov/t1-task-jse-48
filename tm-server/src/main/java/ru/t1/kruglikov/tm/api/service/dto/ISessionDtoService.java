package ru.t1.kruglikov.tm.api.service.dto;

import ru.t1.kruglikov.tm.dto.model.SessionDTO;

public interface ISessionDtoService extends IUserOwnedDtoService<SessionDTO> {
}


