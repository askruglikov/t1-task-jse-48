package ru.t1.kruglikov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.kruglikov.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

    void removeAll();

    void removeAll(@NotNull String userId);

}

