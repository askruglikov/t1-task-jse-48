package ru.t1.kruglikov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.kruglikov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.kruglikov.tm.api.service.IDomainService;
import ru.t1.kruglikov.tm.api.service.ILocatorService;
import ru.t1.kruglikov.tm.dto.request.data.*;
import ru.t1.kruglikov.tm.dto.response.data.*;
import ru.t1.kruglikov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.kruglikov.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final ILocatorService locatorService) {
        super(locatorService);
    }

    @NotNull @Override @WebMethod
    public DataBackupLoadResponse backupLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataBackupLoad();
        return new DataBackupLoadResponse();
    }

    @NotNull @Override @WebMethod
    public DataBackupSaveResponse backupSave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupSaveRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataBackupSave();
        return new DataBackupSaveResponse();
    }

    @NotNull @Override @WebMethod
    public DataBase64LoadResponse base64Load(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64LoadRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataBase64Load();
        return new DataBase64LoadResponse();
    }

    @NotNull @Override @WebMethod
    public DataBase64SaveResponse base64Save(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64SaveRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataBase64Save();
        return new DataBase64SaveResponse();
    }

    @NotNull @Override @WebMethod
    public DataBinaryLoadResponse binaryLoad(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinaryLoadRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataBinaryLoad();
        return new DataBinaryLoadResponse();
    }

    @NotNull @Override @WebMethod
    public DataBinarySaveResponse binarySave(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinarySaveRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataBinarySave();
        return new DataBinarySaveResponse();
    }

    @NotNull @Override @WebMethod
    public DataJsonLoadFasterXmlResponse jsonLoadFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataJsonLoadFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @NotNull @Override @WebMethod
    public DataJsonLoadJaxBResponse jsonLoadJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonLoadJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataJsonLoadJaxB();
        return new DataJsonLoadJaxBResponse();
    }

    @NotNull @Override @WebMethod
    public DataJsonSaveFasterXmlResponse jsonSaveFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataJsonSaveFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @NotNull @Override @WebMethod
    public DataJsonSaveJaxBResponse jsonSaveJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonSaveJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataJsonSaveJaxB();
        return new DataJsonSaveJaxBResponse();
    }

    @NotNull @Override @WebMethod
    public DataXmlLoadFasterXmlResponse xmlLoadFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataXmlLoadFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @NotNull @Override @WebMethod
    public DataXmlLoadJaxBResponse xmlLoadJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlLoadJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataXmlLoadJaxB();
        return new DataXmlLoadJaxBResponse();
    }

    @NotNull @Override @WebMethod
    public DataXmlSaveFasterXmlResponse xmlSaveFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataXmlSaveFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @NotNull @Override @WebMethod
    public DataXmlSaveJaxBResponse xmlSaveJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlSaveJaxBRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataXmlSaveJaxB();
        return new DataXmlSaveJaxBResponse();
    }

    @NotNull @Override @WebMethod
    public DataYamlLoadFasterXmlResponse yamlLoadFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlLoadFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataYamlLoadFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

    @NotNull @Override @WebMethod
    public DataYamlSaveFasterXmlResponse yamlSaveFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlSaveFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getLocatorService().getDomainService().dataYamlSaveFasterXml();
        return new DataYamlSaveFasterXmlResponse();
    }

}
