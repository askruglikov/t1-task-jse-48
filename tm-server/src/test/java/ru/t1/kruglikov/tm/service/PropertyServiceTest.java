package ru.t1.kruglikov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.kruglikov.tm.api.service.IPropertyService;
import ru.t1.kruglikov.tm.service.PropertyService;
import ru.t1.kruglikov.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class PropertyServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Test
    public void testPropertyGetServerHost() {
        Assert.assertNotNull(propertyService.getServerHost());
    }

    @Test
    public void testPropertyGetServerPort() {
        Assert.assertNotNull(propertyService.getServerPort());
    }

    @Test
    public void testPropertyGetSessionKey() {
        Assert.assertNotNull(propertyService.getSessionKey());
    }

    @Test
    public void testPropertyGetSessionTimeout() {
        Assert.assertNotNull(propertyService.getSessionTimeout());
    }

}
